# New Generation Elevator

이번 졸업프로젝트 과제로 만들고자 하는 것은 강화학습을 활용한 엘레베이터 알고리즘 개선이다. 엘레베이터를 효율적으로 개선하기 위해서는 두가지 방법이 존재한다. <br>

1. 엘레베이터의 이동경로. 즉, 명령이 stack으로 이루어져 있을 때, 어떤 순서로 실행하는 것이 좋을까?
2. 사용자의 예측. 즉 이전 정보를 바탕으로 어떤 시점에 몇 층에서 사용자가 들어올 것인지를 예상해, 사용자가 시그널을 주기 전에 미리 엘레베이터가 도착하게 하여 효율성을 개선하는 것이다.

이번 프로젝트에서는 아마 (2)번을 중점적으로 개선할 것이다. 물론 (2)번이 효과적으로 완성된다면, 완성된 dataset을 기반으로 보상 학습을 기반으로 하여 (1)번을 완성할 수도 있을 것이다. 하지만 시간상의 한계로 거기까지 할 지는 미지수이다.

## Index

1. [Project Milestones](#Project-Milestones)
2. [Expected BottleNeck](#Expected-BottleNeck)
3. [Scheduling Criteria](#Scheduling-Criteria)
4. [What needs to be decided?](#What-needs-to-be-decided?)
5. [TA FeedBack](#TA-FeedBack)
6. [Study Record](#Study-Record)
7. [참조 링크](#참조-링크)

## Project Milestones

1. 엘레베이터의 보상 시스템을 설계한다. 즉 어떤 결과가 더 나은 결과인지를 알려준다.
2. 간단한 시뮬레이터를 설계한다. 즉 데이터셋과 예측 데이터셋을 주었을 때 해당 보상 시스템에 따른 결과를 알려준다.

- 다시 말해서 해당 엘레베이터의 스펙(엘레베이터의 속도, 갯수, 들어가고 나서 닫힐때까지의 평균 속도..)이 주어졌을 때, 주어지는 데이터셋과 예측한 데이터셋을 비교하여, 해당 예측이 어느정도 효과적인지 알려주어야 한다.

3. 공부했던 것을 바탕으로, 수학적으로 데이터셋을 어떻게 모델링할지를 결정한다. 학습을 시킨 모델과, branch predictor의 모델과 비슷한 방식으로 예측한 hard coded predictor을 비교한다. (이 부분은 확실하게 정해지지 않았음.)

## Scheduling Criteria

- provide even service to each floor
- minimize how long passengers wait for an elevator to arrive
- minimize how long passengers spend to get to their destination floor
- serve as many passengers as possible

## Expected BottleNeck

1. 엘레베이터의 데이터셋을 받는 것이 가장 큰 난관이다. 개인적으로는 301동의 데이터와 관정의 데이터로 하는것이 목표인데, 조금 더 직관적인 개선방안을 보여줄 수 있기 때문이다.
2. 공부해야 하는 분야를 정하고, 최대한 빠른 시간 안에 학습을 완료해야 한다. 2달이라는 매우 짧은 시간동안 완성해야하기 때문이다.

## What needs to be decided?

1. 어떤 언어 환경에서 작성할 것인가? (Python, C, C++, verilog??)
2. milestone의 order.
3. 어떤 책으로 공부할 것인가.

## TA FeedBack

논문에서 데이터셋 어떻게 찾아봤는지.

## Study Record

### 1월 0주차

1.  (12/31) Sutton 책을 3장 공부했으나, 진도가 생각처럼 나가지 않아 CS234 강의로 공부하기로 결정함. 총 11강으로 이루어져있는데 하루 한강씩 듣는것을 우선적인 목표로 한다.
2.  (1/2/2020) CS234 3강 들을 생각이지만... 다 못 들을 수도...? 현대엘리베이터 회사에 이메일로 문의 넣었다. 긍정적으로 답변해 주시면 좋겠다.. 논문들을 찾아봤는데, 주로 multi-elevator일 때 무작위로 많은 수의 요청이 발생할 때에 맞추어 이 NP 문제를 푸는 방식으로 해결함. 즉 다시 말해서 일반적으로는 시뮬레이터를 통하여서 해결했다는 뚯.
    > This da-ta is collected from a simulator denominated Elevator and using three different passenger profiles, up-peak, down-peak and interfloor.<br> [Predicting the passenger request in the elevator dis-patching problem](https://s3.amazonaws.com/academia.edu.documents/45795223/Predicting_the_passenger_request_in_the_elevator_dispatching_problem.pdf?response-content-disposition=inline%3B%20filename%3DPredicting_the_Passenger_Request_in_the.pdf&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWOWYYGZ2Y53UL3A%2F20200102%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20200102T060012Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=98e1cd46a792404d5a22f0c3973919f60f770c013f6b6e70e0ecddef16950909)

## 참조 링크

> [Markdown Syntax](https://heropy.blog/2017/09/30/markdown/) <br> [Elevator Scheduling](http://www.columbia.edu/~cs2035/courses/ieor4405.S13/p14.pdf)<br> [Is there any public elevator scheduling algorithm standard?](https://www.quora.com/Is-there-any-public-elevator-scheduling-algorithm-standard)<br> [CS234](https://drive.google.com/file/d/1f6KY9N1Fyhm7CO3SELZnD_WLcghAcROD/view) <br>[This da-ta is collected from a simulator denominated Elevator and using three different passenger profiles, up-peak, down-peak and interfloor.]
